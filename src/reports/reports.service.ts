import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateReportDTO } from './dtos/create-report.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Report } from './report.entity';
import { Repository } from 'typeorm';
import { User } from 'src/users/user.entity';
import { GetEstimateDTO } from './dtos/get-estimate.dto';

@Injectable()
export class ReportsService {
  constructor(@InjectRepository(Report) private repo: Repository<Report>) {}

  create(body: CreateReportDTO, user: User) {
    const report = this.repo.create(body);
    report.user = user;
    return this.repo.save(report);
  }

  async changeApproval(id: string, approved: boolean) {
    const report = await this.repo.findOne({ where: { id: parseInt(id) } });
    if (!report) {
      throw new NotFoundException('Reporte no encontrado');
    }
    report.approved = approved;
    return this.repo.save(report);
  }

  async estimate(estimateDTO: GetEstimateDTO) {
    return this.repo
      .createQueryBuilder()
      .select('AVG(price)', 'price')
      .where('make = :make', { make: estimateDTO.make })
      .andWhere('model = :model', { model: estimateDTO.model })
      .andWhere('lng - :lng BETWEEN -5 AND 5', { lng: estimateDTO.lng })
      .andWhere('lat - :lng BETWEEN -5 AND 5', { lat: estimateDTO.lat })
      .andWhere('year - :year BETWEEN -3 AND 3', { year: estimateDTO.year })
      .andWhere('approved IS TRUE')
      .orderBy('mileage - :mileage', 'DESC')
      .setParameters({ mileage: estimateDTO.mileage })
      .limit(3)
      .getRawOne();
  }
}
